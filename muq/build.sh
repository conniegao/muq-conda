
#cmake -DCMAKE_OSX_DEPLOYMENT_TARGET=10.11 -DMUQ_BOOST_DIR=${PREFIX} -DMUQ_HDF5_DIR=${PREFIX} -DMUQ_SUNDIALS_DIR=${PREFIX} -DCMAKE_INSTALL_PREFIX=${PREFIX} ${SRC_DIR}

echo ${SRC_DIR}
echo ${PREFIX}


cmake    \
    -DCMAKE_INSTALL_PREFIX=${PREFIX} \
    -DMUQ_HDF5_DIR=${PREFIX} \
    -DMUQ_BOOST_DIR=${PREFIX} \
    -DMUQ_SUNDIALS_DIR=${PREFIX} \
    -DMUQ_FLANN_DIR=${PREFIX} \
    -DMUQ_EIGEN3_DIR=${PREFIX}/include \
    -DMUQ_USE_PYTHON=ON \
    ${SRC_DIR}/MUQ

make -j${CPU_COUNT} install
